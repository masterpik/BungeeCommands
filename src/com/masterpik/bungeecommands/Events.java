package com.masterpik.bungeecommands;

import com.masterpik.api.json.*;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilJson;
import com.masterpik.api.util.UtilUUID;
import com.masterpik.bungeecommands.aliases.Aliases;
import com.masterpik.bungeecommands.friends.FriendsManagement;
import com.masterpik.bungeecommands.help.HelpManagement;
import com.masterpik.bungeeconnect.database.UUIDName;
import com.masterpik.bungeemanagement.commands.CommandsPerms;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;
import net.md_5.bungee.protocol.packet.Chat;

import java.net.Proxy;
import java.util.ArrayList;
import java.util.UUID;

public class Events implements Listener{

    @EventHandler
    public void TabCompleteEvent(TabCompleteEvent event) {
        //ProxyServer.getInstance().getLogger().info("TabCompleteEvent : "+ event.toString());

        /*if (!event.getSuggestions().isEmpty()) {
            return; //If suggestions for this command are handled by other plugin don't add any
        }*/

        String[] args = event.getCursor().split(" ");

        if (!args[0].startsWith("/")) {
            return;
        }

        final String checked = (args.length > 0 ? args[args.length - 1] : event.getCursor()).toLowerCase();

        if (event.getSender() instanceof ProxiedPlayer) {


            if (CommandsPerms.hasPermission(((ProxiedPlayer) event.getSender()).getUniqueId().toString(), args[0])){

                if (args[0].startsWith("/")) {
                    args[0] = args[0].replaceFirst("/", "");
                }

                event.getSuggestions().clear();

                args[0] = Aliases.getCommandFromAlias(args[0]);

                if (args[0].equalsIgnoreCase("msg")
                        /*|| Aliases.getAliases("msg").contains(args[0].toLowerCase())*/) {
                    for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                        if (player.getName().toLowerCase().startsWith(checked)) {
                            event.getSuggestions().add(player.getName());
                        }
                    }
                    if (event.getSender() instanceof ProxiedPlayer
                            && event.getSuggestions().contains(((ProxiedPlayer) event.getSender()).getName())) {
                        event.getSuggestions().remove(((ProxiedPlayer) event.getSender()).getName());
                    }

                } else if (args[0].equalsIgnoreCase("friends")
                        /*|| Aliases.getAliases("friends").contains(args[0].toLowerCase())*/) {

                    if (args.length > 1 && args.length < 4) {

                        if (args[1].equalsIgnoreCase("invite")
                                || args[1].equalsIgnoreCase("add")) {
                            for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                                if (player.getName().toLowerCase().startsWith(checked)) {
                                    event.getSuggestions().add(player.getName());
                                }
                            }
                            if (event.getSender() instanceof ProxiedPlayer
                                    && event.getSuggestions().contains(((ProxiedPlayer) event.getSender()).getName())) {
                                event.getSuggestions().remove(((ProxiedPlayer) event.getSender()).getName());
                            }
                        } else if (args[1].equalsIgnoreCase("delete")
                                || args[1].equalsIgnoreCase("remove")) {
                            for (UUID player : FriendsManagement.friendsList.get(((ProxiedPlayer) event.getSender()).getUniqueId())) {
                                if (UUIDName.uuidToPlayerName(player).startsWith(checked)) {
                                    event.getSuggestions().add(UUIDName.uuidToPlayerName(player));
                                }
                            }
                        } else if (args[1].equalsIgnoreCase("accept")
                                || args[1].equalsIgnoreCase("acept")) {
                            for (UUID player : FriendsManagement.invitations.get(((ProxiedPlayer) event.getSender()).getUniqueId())) {
                                if (UUIDName.uuidToPlayerName(player).startsWith(checked)) {
                                    event.getSuggestions().add(UUIDName.uuidToPlayerName(player));
                                }
                            }
                        }

                    }

                } else if (args[0].equalsIgnoreCase("pik")
                        || args[0].equalsIgnoreCase("stats")
                        /*|| Aliases.getAliases("stats").contains(args[0].toLowerCase())*/) {
                    for (String player : UUIDName.uuidTable.values()) {
                        if (player.toLowerCase().startsWith(checked)) {
                            event.getSuggestions().add(player);
                        }
                    }
                } else if (args[0].equalsIgnoreCase("help")
                        /*|| Aliases.getAliases("help").contains(args[0].toLowerCase())*/) {
                    for (String cmd : HelpManagement.helps.keySet()) {
                        if (cmd.toLowerCase().startsWith(checked)) {
                            event.getSuggestions().add(cmd);
                        }
                    }
                }
            } else {
                event.setCancelled(true);
                //((ProxiedPlayer) event.getSender()).sendMessage(new TextComponent("cette commandes ne t'es pas autorisée"));
            }
        }

        //ProxyServer.getInstance().getLogger().info("TabCompleteEvent with fisrt args : "+args[0]);

    }

    @EventHandler
    public void PlayerDisconnectEvent(PlayerDisconnectEvent event) {

        ProxiedPlayer player = event.getPlayer();

        if (FriendsManagement.invitations.containsKey(player.getUniqueId())) {
            FriendsManagement.invitations.remove(player.getUniqueId());
        }

        if (FriendsManagement.friendsList.containsKey(player.getUniqueId())
                && !FriendsManagement.friendsList.get(player.getUniqueId()).isEmpty()) {

            ChatText prefix = new ChatText(FriendsManagement.prefix);

            ArrayList<UUID> list = new ArrayList<>();
            list.addAll(FriendsManagement.friendsList.get(player.getUniqueId()));

            for (UUID pl : list) {

                ChatText info = new ChatText("");
                HoverEvent hv = null;
                ClickEvent cv = null;

                if (ProxyServer.getInstance().getPlayer(pl) == null) {
                } else {

                    ProxyServer.getInstance().getPlayer(pl).unsafe().sendPacket(
                            new Chat(
                                    UtilJson.getJsonFromObject(
                                            new Object[]{"",
                                                    new ChatText(prefix),
                                                    new ChatText(" Ton ami ").setExtra(new Object[]{"", new ChatText(player.getName()).setColor(Colors.AQUA).setItalic(true), " s'est déconnecté"}).setColor(Colors.RED)})));


                }

            }

        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void ServerConnectEvent(ServerConnectEvent event) {

        if (!event.isCancelled()) {

            if (event.getPlayer().getServer() == null) {

                ProxiedPlayer player = event.getPlayer();

                if (FriendsManagement.friendsList.containsKey(player.getUniqueId())
                        && !FriendsManagement.friendsList.get(player.getUniqueId()).isEmpty()) {

                    ChatText prefix = new ChatText(FriendsManagement.prefix);

                    ArrayList<UUID> list = new ArrayList<>();
                    list.addAll(FriendsManagement.friendsList.get(player.getUniqueId()));

                    int friendsOnline = 0;

                    for (UUID pl : list) {
                        if (ProxyServer.getInstance().getPlayer(pl) != null) {
                            friendsOnline++;
                        }
                    }

                    if (friendsOnline > 0) {

                        player.unsafe().sendPacket(
                                new Chat(
                                        UtilJson.getJsonFromObject(
                                                new Object[]{"",
                                                        prefix,
                                                        new ChatText(" Amis en ligne ").setExtra(new Object[]{"", new ChatText("(" + Integer.toString(friendsOnline) + ")").setBold(true).setColor(Colors.GREEN), " : "}).setColor(Colors.GOLD)})));
                    } else {
                        player.unsafe().sendPacket(
                                new Chat(
                                        UtilJson.getJsonFromObject(
                                                new Object[]{"",
                                                        prefix,
                                                        new ChatText(" Aucun amis en ligne").setColor(Colors.GOLD)})));
                    }

                    for (UUID pl : list) {

                        ChatText info = new ChatText("");
                        ChatText info2 = new ChatText("");
                        HoverEvent hv = null;
                        ClickEvent cv = null;

                        if (ProxyServer.getInstance().getPlayer(pl) == null) {
                            //info = info.setText(" ●(hors ligne)").setColor(Colors.RED).setBold(true);
                        } else {
                            info = info.setText(" (serveur " + ProxyServer.getInstance().getPlayer(pl).getServer().getInfo().getName() + ")").setColor(Colors.GREEN).setBold(true);
                            info2 = info2.setText("● ").setColor(Colors.GREEN).setBold(true);
                            cv = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/msg " + ProxyServer.getInstance().getPlayer(pl).getName() + " ");
                            hv = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/msg " + ProxyServer.getInstance().getPlayer(pl).getName()).setColor(Colors.GREEN).setBold(true));

                            player.unsafe().sendPacket(
                                    new Chat(
                                            UtilJson.getJsonFromObject(
                                                    new Object[]{"",
                                                            new ChatText("  - ").setExtra(new Object[]{"",info2,
                                                                    new ChatText(UUIDName.uuidToPlayerName(pl)).setColor(Colors.AQUA).setItalic(true),
                                                                    info}).setColor(Colors.GOLD).setClickEvent(cv).setHoverEvent(hv)})));

                            ClickEvent cv2 = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/msg " + player.getName() + " ");
                            HoverEvent hv2 = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/msg " + player.getName()).setColor(Colors.GREEN).setBold(true));

                            ProxyServer.getInstance().getPlayer(pl).unsafe().sendPacket(
                                    new Chat(
                                            UtilJson.getJsonFromObject(
                                                    new Object[]{"",
                                                            new ChatText(prefix).setClickEvent(cv2).setHoverEvent(hv2),
                                                            new ChatText(" Ton ami ").setExtra(new Object[]{"", new ChatText(player.getName()).setColor(Colors.AQUA).setItalic(true), " s'est connecté"}).setColor(Colors.GREEN).setClickEvent(cv2).setHoverEvent(hv2)})));


                        }

                    }


                }
            }
        }

    }

}
