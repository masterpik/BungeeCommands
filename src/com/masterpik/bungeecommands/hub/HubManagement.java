package com.masterpik.bungeecommands.hub;

import com.masterpik.bungeecommands.Main;
import com.masterpik.bungeecommands.aliases.Aliases;
import com.masterpik.bungeecommands.privatemessages.CommandsMessages;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.PluginManager;

import java.util.ArrayList;

public class HubManagement {

    public static void hubInit() {
        initCommmands();
    }

    public static void initCommmands() {
        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();

        String name = "hub";

        ArrayList<String> alias = new ArrayList<>();
        alias.add("lobby");
        alias.add(name);
        for (String cmd : alias) {
            pluginManager.registerCommand(Main.plugin, new CommandsHub(cmd));
        }
        alias.remove(name);

        Aliases.addCommand(name, alias);
    }
}
