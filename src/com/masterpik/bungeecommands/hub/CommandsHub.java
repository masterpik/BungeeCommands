package com.masterpik.bungeecommands.hub;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CommandsHub extends Command {
    public CommandsHub(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.getServer().getInfo().getName().equalsIgnoreCase("hub")) {
                player.chat("/spawn");
            }
            else {
                player.connect(ProxyServer.getInstance().getServerInfo("hub"));
            }


        }
    }
}
