package com.masterpik.bungeecommands.party;

import com.masterpik.api.players.MasterpikPlayer;
import com.masterpik.api.players.Players;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;

public class Party {

    protected int id;

    protected ProxiedPlayer master;

    protected byte maxPlayers;

    protected ArrayList<ProxiedPlayer> players;

    public Party(int id, ProxiedPlayer master) {
        setId(id);
        setMaster(master);

        this.players = new ArrayList<>();
        this.players.add(master);

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProxiedPlayer getMaster() {
        return master;
    }

    public void setMaster(ProxiedPlayer master) {
        this.master = master;
        setMaxPlayers(Players.players.get(master.getUniqueId()).getStatu().getMaxPartyPlayer());
    }

    public ArrayList<ProxiedPlayer> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<ProxiedPlayer> players) {
        this.players = players;
    }

    public byte getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(byte maxPlayers) {
        this.maxPlayers = maxPlayers;
    }


    @Override
    public String toString() {
        return "Party{" +
                "id=" + id +
                ", master=" + master +
                ", maxPlayers=" + maxPlayers +
                ", players=" + players +
                '}';
    }
}
