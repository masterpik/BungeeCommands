package com.masterpik.bungeecommands.friends;

import com.masterpik.api.json.*;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilJson;
import com.masterpik.bungeecommands.aliases.Aliases;
import com.masterpik.bungeecommands.privatemessages.CommandsMessages;
import com.masterpik.bungeeconnect.database.UUIDName;
import com.masterpik.database.api.Query;
import com.masterpik.database.bungee.Main;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.PluginManager;
import net.md_5.bungee.protocol.packet.Chat;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class FriendsManagement {

    public static HashMap<UUID, ArrayList<UUID>> friendsList;
    public static HashMap<UUID, ArrayList<UUID>> invitations;

    public static ChatText prefix;

    public static Object[] help;


    public static void FriendsInit() {

        initCommmands();

        friendsList = new HashMap<>();
        friendsList.putAll(getFriendsList());

        invitations = new HashMap<>();


        prefix = new ChatText("▌").setExtra(new Object[]{"", new ChatText("FRIENDS").setBold(true).setColor(Colors.GOLD), "▌"}).setBold(true).setColor(Colors.BLACK);

        HoverEvent hv = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/friends").setBold(true).setColor(Colors.GREEN));
        ClickEvent cv = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends ");

        help = new Object[]{"",
                new ChatText(prefix).setClickEvent(cv).setHoverEvent(hv),
                new ChatText("\n - /friends invite [joueur]").
                        setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText[] {new ChatText("Invite un joueur à être ton amis !\n").setColor(Colors.GOLD).setBold(true), new ChatText("/friends invite").setBold(true).setColor(Colors.GREEN)})).
                        setClickEvent(new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends invite ")).setItalic(true).setColor(Colors.GOLD),
                new ChatText("\n - /friends remove [joueur]").
                        setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText[] {new ChatText("Enlève un joueur de ta liste d'amis !\n").setColor(Colors.GOLD).setBold(true), new ChatText("/friends remove").setBold(true).setColor(Colors.GREEN)})).
                        setClickEvent(new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends remove ")).setItalic(true).setColor(Colors.GOLD),
                new ChatText("\n - /friends accept [joueur]").
                        setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText[] {new ChatText("Accepte l'invitation d'un joueur !\n").setColor(Colors.GOLD).setBold(true), new ChatText("/friends accept").setBold(true).setColor(Colors.GREEN)})).
                        setClickEvent(new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends accept ")).setItalic(true).setColor(Colors.GOLD),
                new ChatText("\n - /friends list").
                        setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText[] {new ChatText("Regarde ta liste d'amis !\n").setColor(Colors.GOLD).setBold(true), new ChatText("/friends list").setBold(true).setColor(Colors.GREEN)})).
                        setClickEvent(new ClickEvent(ClickEvents.RUN_COMMAND, "/friends list")).setItalic(true).setColor(Colors.GOLD),
                new ChatText("\n - /friends help [command]").
                        setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText[] {new ChatText("Regarde l'aide pour une commande !\n").setColor(Colors.GOLD).setBold(true), new ChatText("/friends help").setBold(true).setColor(Colors.GREEN)})).
                        setClickEvent(new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends help ")).setItalic(true).setColor(Colors.GOLD)};

    }


    public static void initCommmands() {
        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();

        String name = "friends";

        ArrayList<String> alias = new ArrayList<>();
        alias.add("friend");
        alias.add("amis");
        alias.add("ami");
        alias.add("f");
        alias.add(name);
        for (String cmd : alias) {
            pluginManager.registerCommand(com.masterpik.bungeecommands.Main.plugin, new CommandsFriends(cmd));
        }
        alias.remove(name);

        Aliases.addCommand(name, alias);
    }

    public static boolean addFriends(UUID uuid1, UUID uuid2) {

        if (!friendsList.containsKey(uuid1)) {
            friendsList.put(uuid1, new ArrayList<>());
        }
        if (!friendsList.get(uuid1).contains(uuid2)) {
            friendsList.get(uuid1).add(uuid2);
        } else {
            return false;
        }

        if (!friendsList.containsKey(uuid2)) {
            friendsList.put(uuid2, new ArrayList<>());
        }
        if (!friendsList.get(uuid2).contains(uuid1)) {
            friendsList.get(uuid2).add(uuid1);
        } else {
            return false;
        }

        String query1 = "INSERT INTO masterpik.friends (friends1, friends2) VALUES ('" + uuid1.toString() + "', '" + uuid2.toString() + "')";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
        return Query.queryInput(Main.dbConnection, query1);

        //return true;
    }

    public static boolean removeFriends(UUID uuid1, UUID uuid2) {
        if (!friendsList.containsKey(uuid1)) {
            return false;
        }
        if (!friendsList.get(uuid1).contains(uuid2)) {
            return false;
        }

        friendsList.get(uuid1).remove(uuid2);

        if (friendsList.get(uuid1).size() == 0) {
            friendsList.remove(uuid1);
        }


        if (!friendsList.containsKey(uuid2)) {
            return false;
        }
        if (!friendsList.get(uuid2).contains(uuid1)) {
            return false;
        }

        friendsList.get(uuid2).remove(uuid1);

        if (friendsList.get(uuid2).size() == 0) {
            friendsList.remove(uuid2);
        }


        String query1 = "DELETE FROM masterpik.friends WHERE (friends1 = '" + uuid1.toString()+"' AND friends2 = '"+uuid2.toString()+"') OR (friends1 = '" + uuid2.toString()+"' AND friends2 = '"+uuid1.toString()+"')";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
        if (Query.queryInput(Main.dbConnection, query1)) {

            HoverEvent hv1 = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/friends ").setBold(true).setColor(Colors.GREEN));
            ClickEvent cv1 = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends ");


            if (ProxyServer.getInstance().getPlayer(uuid1) != null) {
                ProxyServer.getInstance().getPlayer(uuid1).unsafe().sendPacket(new Chat(
                        UtilJson.getJsonFromObject(
                                new Object[]{"",
                                        prefix.setClickEvent(cv1).setHoverEvent(hv1),
                                        new ChatText(" Le joueur ").setExtra(
                                                new Object[]{"",
                                                        new ChatText(UUIDName.uuidToPlayerName(uuid2)).setColor(Colors.AQUA).setBold(false).setItalic(true),
                                                        " n'est plus votre amis"}).setColor(Colors.RED)})));
            }

            if (ProxyServer.getInstance().getPlayer(uuid2) != null) {
                ProxyServer.getInstance().getPlayer(uuid2).unsafe().sendPacket(new Chat(
                        UtilJson.getJsonFromObject(
                                new Object[]{"",
                                        prefix.setClickEvent(cv1).setHoverEvent(hv1),
                                        new ChatText(" Le joueur ").setExtra(
                                                new Object[]{"",
                                                        new ChatText(UUIDName.uuidToPlayerName(uuid1)).setColor(Colors.AQUA).setBold(false).setItalic(true),
                                                        " n'est plus votre amis"}).setColor(Colors.RED)})));
            }

            return true;

        } else {
            return false;
        }

    }



    public static boolean addInvitation(UUID player, UUID invite) {

        if (!invitations.containsKey(player)) {
            invitations.put(player, new ArrayList<>());
        }
        if (!invitations.get(player).contains(invite)) {
            invitations.get(player).add(invite);
        } else {
            return false;
        }


        return true;
    }

    public static boolean sendInvitation(UUID player, UUID invite) {

        if (ProxyServer.getInstance().getPlayer(invite) == null) {
            return false;
        }



        if (addInvitation(player, invite)) {

            HoverEvent hv1 = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/msg " + ProxyServer.getInstance().getPlayer(invite).getName()).setBold(true).setColor(Colors.GREEN));
            ClickEvent cv1 = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/msg " + ProxyServer.getInstance().getPlayer(invite).getName() + " ");

            ProxyServer.getInstance().getPlayer(player).unsafe().sendPacket(
                    new Chat(
                            UtilJson.getJsonFromObject(
                                    new Object[]{"",
                                            prefix.setClickEvent(cv1).setHoverEvent(hv1),
                                            new ChatText(" Invitation envoyée à ").setExtra(
                                                    new Object[]{"",
                                                            new ChatText(ProxyServer.getInstance().getPlayer(invite).getName()).setColor(Colors.AQUA).setBold(false).setItalic(true)}).setColor(Colors.GOLD)})));


            HoverEvent hv2 = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/friends accept " + ProxyServer.getInstance().getPlayer(player).getName()).setBold(true).setColor(Colors.GREEN));
            ClickEvent cv2 = new ClickEvent(ClickEvents.RUN_COMMAND, "/friends accept " + ProxyServer.getInstance().getPlayer(player).getName());

            ProxyServer.getInstance().getPlayer(invite).unsafe().sendPacket(
                    new Chat(
                            UtilJson.getJsonFromObject(
                                    new Object[]{"",
                                            prefix.setClickEvent(cv2).setHoverEvent(hv2),
                                            new ChatText(" Le joueur ").setExtra(
                                                    new Object[]{"",
                                                            new ChatText(ProxyServer.getInstance().getPlayer(player).getName()).setColor(Colors.AQUA).setBold(false).setItalic(true),
                                                            " t'invite à être son amis",
                                                            new ChatText(" (clique sur le message pour accepter)").setItalic(true).setColor(Colors.GREEN)}).setBold(true).setColor(Colors.GOLD).setHoverEvent(hv2).setClickEvent(cv2)})));
        } else {

            HoverEvent hv1 = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/msg " + ProxyServer.getInstance().getPlayer(invite).getName()).setBold(true).setColor(Colors.GREEN));
            ClickEvent cv1 = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/msg " + ProxyServer.getInstance().getPlayer(invite).getName() + " ");

            ProxyServer.getInstance().getPlayer(player).unsafe().sendPacket(
                    new Chat(
                            UtilJson.getJsonFromObject(
                                    new Object[]{"",
                                            prefix.setClickEvent(cv1).setHoverEvent(hv1),
                                            new ChatText(" Le joueur ").setExtra(
                                                    new Object[]{"",
                                                            new ChatText(ProxyServer.getInstance().getPlayer(invite).getName()).setColor(Colors.AQUA).setBold(false).setItalic(true),
                                                            " est déjà votre amis"}).setBold(true).setColor(Colors.RED)})));

            return false;
        }

        return true;
    }

    public static boolean acceptInvitation(UUID player, UUID invite) {

        if (!invitations.containsKey(player)) {
            return false;
        } else if (!invitations.get(player).contains(invite)) {
            return false;
        }

        if (ProxyServer.getInstance().getPlayer(player) == null) {
            return false;
        }

        addFriends(player, invite);

        HoverEvent hv1 = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/msg "+ProxyServer.getInstance().getPlayer(invite).getName()).setBold(true).setColor(Colors.GREEN));
        ClickEvent cv1 = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/msg "+ProxyServer.getInstance().getPlayer(invite).getName()+" ");

        ProxyServer.getInstance().getPlayer(player).unsafe().sendPacket(
                new Chat(
                        UtilJson.getJsonFromObject(
                                new Object[]{"",
                                        prefix.setClickEvent(cv1).setHoverEvent(hv1),
                                        new ChatText(" Le joueur ").setExtra(
                                                new Object[]{"",
                                                        new ChatText(ProxyServer.getInstance().getPlayer(invite).getName()).setColor(Colors.AQUA).setBold(false).setItalic(true),
                                                        " est désormais votre amis"}).setColor(Colors.GREEN)})));



        HoverEvent hv2 = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/msg "+ProxyServer.getInstance().getPlayer(player).getName()).setBold(true).setColor(Colors.GREEN));
        ClickEvent cv2 = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/msg "+ProxyServer.getInstance().getPlayer(player).getName()+" ");

        ProxyServer.getInstance().getPlayer(invite).unsafe().sendPacket(
                new Chat(
                        UtilJson.getJsonFromObject(
                                new Object[]{"",
                                        prefix.setClickEvent(cv2).setHoverEvent(hv2),
                                        new ChatText(" Le joueur ").setExtra(
                                                new Object[]{"",
                                                        new ChatText(ProxyServer.getInstance().getPlayer(player).getName()).setColor(Colors.AQUA).setBold(false).setItalic(true),
                                                        " est désormais votre amis"}).setColor(Colors.GREEN)})));

        return true;
    }

    public static boolean removeInvitation(UUID player, UUID invite) {
        if (!invitations.containsKey(player)) {
            return false;
        }
        if (!invitations.get(player).contains(invite)) {
            return false;
        }

        invitations.get(player).remove(invite);

        if (invitations.get(player).size() == 0) {
            invitations.remove(player);
        }

        return true;
    }



    private static HashMap<UUID, ArrayList<UUID>> getFriendsList() {
        HashMap<UUID, ArrayList<UUID>> friendsL = new HashMap<>();

        String query1 = "SELECT * FROM masterpik.friends";
        //ProxyServer.getInstance().getLogger().info("QUERY :::: "+query1);
        ResultSet result1 = Query.queryOutpout(Main.dbConnection, query1);

        try {

            while(result1.next()) {

                UUID uuid1 =  (UUID) result1.getObject("friends1");
                UUID uuid2 =  (UUID) result1.getObject("friends2");

                if (!friendsL.containsKey(uuid1)) {
                    friendsL.put(uuid1, new ArrayList<>());
                }
                if (!friendsL.get(uuid1).contains(uuid2)) {
                    friendsL.get(uuid1).add(uuid2);
                }

                if (!friendsL.containsKey(uuid2)) {
                    friendsL.put(uuid2, new ArrayList<>());
                }
                if (!friendsL.get(uuid2).contains(uuid1)) {
                    friendsL.get(uuid2).add(uuid1);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return friendsL;
    }


}
