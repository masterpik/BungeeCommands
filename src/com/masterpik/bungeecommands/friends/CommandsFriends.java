package com.masterpik.bungeecommands.friends;

import com.masterpik.api.json.*;
import com.masterpik.api.logger.BungeeLogger;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilJson;
import com.masterpik.api.util.UtilUUID;
import com.masterpik.bungeeconnect.database.UUIDName;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.protocol.packet.Chat;

import java.awt.*;
import java.util.ArrayList;
import java.util.UUID;

public class CommandsFriends extends Command {

    public CommandsFriends(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {

        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            ChatText prefix = new ChatText(FriendsManagement.prefix);

            if (strings != null && strings.length >= 1) {


                if (strings.length >= 2
                        && (strings[0].equalsIgnoreCase("invite")
                        || strings[0].equalsIgnoreCase("add")
                        || strings[0].equalsIgnoreCase("remove")
                        || strings[0].equalsIgnoreCase("delete")
                        || strings[0].equalsIgnoreCase("acept")
                        || strings[0].equalsIgnoreCase("accept"))){

                    UUID uuid = null;

                    if (UtilUUID.isAnUUID(strings[1])) {
                        uuid = UUID.fromString(strings[1]);
                    } else if (UUIDName.playerNameToUUID(strings[1]) != null) {
                        uuid = UUIDName.playerNameToUUID(strings[1]);
                    }


                    if (uuid != null) {

                        if (strings[0].equalsIgnoreCase("invite")
                                || strings[0].equalsIgnoreCase("add")) {

                            if (FriendsManagement.friendsList.containsKey(player.getUniqueId())
                                    && FriendsManagement.friendsList.get(player.getUniqueId()).contains(uuid)) {

                                HoverEvent hv1 = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/msg " + ProxyServer.getInstance().getPlayer(uuid).getName()).setBold(true).setColor(Colors.GREEN));
                                ClickEvent cv1 = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/msg " + ProxyServer.getInstance().getPlayer(uuid).getName() + " ");

                                player.unsafe().sendPacket(
                                        new Chat(
                                                UtilJson.getJsonFromObject(
                                                        new Object[]{"",
                                                                prefix.setClickEvent(cv1).setHoverEvent(hv1),
                                                                new ChatText(" Le joueur ").setExtra(
                                                                        new Object[]{"",
                                                                                new ChatText(ProxyServer.getInstance().getPlayer(uuid).getName()).setColor(Colors.AQUA).setBold(false).setItalic(true),
                                                                                " est déjà votre amis"}).setBold(true).setColor(Colors.RED)})));

                            } else {

                                if (ProxyServer.getInstance().getPlayer(uuid) != null) {
                                    FriendsManagement.sendInvitation(player.getUniqueId(), uuid);
                                } else {
                                    HoverEvent hv = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/friends invite").setBold(true).setColor(Colors.GREEN));
                                    ClickEvent cv = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends invite ");

                                    player.unsafe().sendPacket(
                                            new Chat(
                                                    UtilJson.getJsonFromObject(
                                                            new Object[]{"",
                                                                    prefix.setClickEvent(cv).setHoverEvent(hv),
                                                                    new ChatText(" Le joueur ").setExtra(
                                                                            new Object[]{"",
                                                                                    new ChatText(strings[1]).setColor(Colors.AQUA).setBold(false).setItalic(true),
                                                                                    " n'est pas connecté au serveur"}).setBold(true).setColor(Colors.RED)})));
                                }
                            }
                        } else if (strings[0].equalsIgnoreCase("remove")
                                || strings[0].equalsIgnoreCase("delete")) {

                            if (FriendsManagement.friendsList.containsKey(player.getUniqueId())
                                    && FriendsManagement.friendsList.get(player.getUniqueId()).contains(uuid)) {
                                FriendsManagement.removeFriends(player.getUniqueId(), uuid);
                            } else {
                                HoverEvent hv = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/friends remove").setBold(true).setColor(Colors.GREEN));
                                ClickEvent cv = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends remove ");

                                player.unsafe().sendPacket(
                                        new Chat(
                                                UtilJson.getJsonFromObject(
                                                        new Object[]{"",
                                                                prefix.setClickEvent(cv).setHoverEvent(hv),
                                                                new ChatText(" Le joueur ").setExtra(
                                                                        new Object[]{"",
                                                                                new ChatText(strings[1]).setColor(Colors.AQUA).setBold(false).setItalic(true),
                                                                                " n'est pas dans ta liste d'amis"}).setBold(true).setColor(Colors.RED)})));
                            }

                        } else if (strings[0].equalsIgnoreCase("accept")
                                || strings[0].equalsIgnoreCase("acept")) {

                            if (FriendsManagement.friendsList.containsKey(player.getUniqueId())
                                    && FriendsManagement.friendsList.get(player.getUniqueId()).contains(uuid)) {

                                HoverEvent hv1 = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/msg " + ProxyServer.getInstance().getPlayer(uuid).getName()).setBold(true).setColor(Colors.GREEN));
                                ClickEvent cv1 = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/msg " + ProxyServer.getInstance().getPlayer(uuid).getName() + " ");

                                player.unsafe().sendPacket(
                                        new Chat(
                                                UtilJson.getJsonFromObject(
                                                        new Object[]{"",
                                                                prefix.setClickEvent(cv1).setHoverEvent(hv1),
                                                                new ChatText(" Le joueur ").setExtra(
                                                                        new Object[]{"",
                                                                                new ChatText(ProxyServer.getInstance().getPlayer(uuid).getName()).setColor(Colors.AQUA).setBold(false).setItalic(true),
                                                                                " est déjà votre amis"}).setBold(true).setColor(Colors.RED)})));

                            } else {

                                if (FriendsManagement.invitations.containsKey(uuid)
                                        && FriendsManagement.invitations.get(uuid).contains(player.getUniqueId())) {

                                    if (ProxyServer.getInstance().getPlayer(uuid) != null) {
                                        FriendsManagement.acceptInvitation(uuid, player.getUniqueId());
                                    } else {
                                        HoverEvent hv = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/friends accept").setBold(true).setColor(Colors.GREEN));
                                        ClickEvent cv = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends accept ");

                                        player.unsafe().sendPacket(
                                                new Chat(
                                                        UtilJson.getJsonFromObject(
                                                                new Object[]{"",
                                                                        prefix.setClickEvent(cv).setHoverEvent(hv),
                                                                        new ChatText(" Le joueur ").setExtra(
                                                                                new Object[]{"",
                                                                                        new ChatText(strings[1]).setColor(Colors.AQUA).setBold(false).setItalic(true),
                                                                                        " n'est pas connecté au serveur"}).setBold(true).setColor(Colors.RED)})));
                                    }
                                } else {
                                    HoverEvent hv = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/friends accept").setBold(true).setColor(Colors.GREEN));
                                    ClickEvent cv = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends accept ");

                                    player.unsafe().sendPacket(
                                            new Chat(
                                                    UtilJson.getJsonFromObject(
                                                            new Object[]{"",
                                                                    prefix.setClickEvent(cv).setHoverEvent(hv),
                                                                    new ChatText(" Le joueur ").setExtra(
                                                                            new Object[]{"",
                                                                                    new ChatText(strings[1]).setColor(Colors.AQUA).setBold(false).setItalic(true),
                                                                                    " ne ta pas invité"}).setBold(true).setColor(Colors.RED)})));
                                }
                            }
                        }

                    } else {
                        HoverEvent hv = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/friends "+strings[0]).setBold(true).setColor(Colors.GREEN));
                        ClickEvent cv = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends "+strings[0]+" ");

                        player.unsafe().sendPacket(
                                new Chat(
                                        UtilJson.getJsonFromObject(
                                                new Object[]{"",
                                                        prefix.setClickEvent(cv).setHoverEvent(hv),
                                                        new ChatText(" Le joueur ").setExtra(
                                                                new Object[]{"",
                                                                        new ChatText(strings[1]).setColor(Colors.AQUA).setBold(false).setItalic(true),
                                                                        " n'existe pas"}).setBold(true).setColor(Colors.RED)})));
                    }


                } else if (strings[0].equalsIgnoreCase("list")) {

                    if (FriendsManagement.friendsList.containsKey(player.getUniqueId())
                            && FriendsManagement.friendsList.get(player.getUniqueId()).size() > 0) {

                        player.unsafe().sendPacket(
                                new Chat(
                                        UtilJson.getJsonFromObject(
                                                new Object[]{"",
                                                        prefix,
                                                        new ChatText(" Amis : ").setColor(Colors.GOLD)})));

                        ArrayList<UUID> list = new ArrayList<>();
                        list.addAll(FriendsManagement.friendsList.get(player.getUniqueId()));

                        for (UUID pl : list) {

                            ChatText info = new ChatText("");
                            ChatText info2 = new ChatText("");
                            HoverEvent hv = null;
                            ClickEvent cv = null;

                            if (ProxyServer.getInstance().getPlayer(pl) == null) {
                                info = info.setText(" (hors ligne)").setColor(Colors.RED).setBold(true);
                                info2 = info2.setText("● ").setColor(Colors.RED).setBold(true);
                            } else {
                                info = info.setText(" (en ligne | serveur "+ProxyServer.getInstance().getPlayer(pl).getServer().getInfo().getName()+")").setColor(Colors.GREEN).setBold(true);
                                info2 = info2.setText("● ").setColor(Colors.GREEN).setBold(true);
                                cv = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/msg "+ProxyServer.getInstance().getPlayer(pl).getName()+" ");
                                hv = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/msg "+ProxyServer.getInstance().getPlayer(pl).getName()).setColor(Colors.GREEN).setBold(true));
                            }

                            player.unsafe().sendPacket(
                                    new Chat(
                                            UtilJson.getJsonFromObject(
                                                    new Object[]{"",
                                                            new ChatText("  - ").setExtra(new Object[]{"",info2,
                                                                                                        new ChatText(UUIDName.uuidToPlayerName(pl)).setColor(Colors.AQUA).setItalic(true),
                                                                                                        info}).setColor(Colors.GOLD).setClickEvent(cv).setHoverEvent(hv)})));
                        }

                    } else {
                        player.unsafe().sendPacket(
                                new Chat(
                                        UtilJson.getJsonFromObject(
                                                new Object[]{"",
                                                        prefix.setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/friends invite").setColor(Colors.GREEN).setBold(true))).setClickEvent(new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends invite ")),
                                                        new ChatText(" Tu n'as pas encore d'amis, pour en inviter utilise /friends invite !").setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/friends invite").setColor(Colors.GREEN).setBold(true))).setClickEvent(new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends invite ")).setBold(true).setColor(Colors.RED)})));
                    }


                } else if (strings[0].equalsIgnoreCase("help")) {

                    if (strings.length >= 2) {

                        if (strings[1].equalsIgnoreCase("invite")
                                || strings[1].equalsIgnoreCase("add")) {

                            HoverEvent hv = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText[] {new ChatText("Invite un joueur à être ton amis !\n").setColor(Colors.GOLD).setBold(true), new ChatText("/friends invite").setBold(true).setColor(Colors.GREEN)});
                            ClickEvent cv = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends invite ");

                            player.unsafe().sendPacket(
                                    new Chat(
                                            UtilJson.getJsonFromObject(
                                                    new Object[]{"",
                                                            prefix.setClickEvent(cv).setHoverEvent(hv),
                                                            new ChatText(" /friends invite [joueur]").setHoverEvent(hv).setClickEvent(cv).setItalic(true).setColor(Colors.GOLD)})));

                        } else if (strings[1].equalsIgnoreCase("remove")
                                || strings[1].equalsIgnoreCase("delete")) {
                            HoverEvent hv = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText[] {new ChatText("Enlève un joueur de ta liste d'amis !\n").setColor(Colors.GOLD).setBold(true), new ChatText("/friends remove").setBold(true).setColor(Colors.GREEN)});
                            ClickEvent cv = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends remove ");

                            player.unsafe().sendPacket(
                                    new Chat(
                                            UtilJson.getJsonFromObject(
                                                    new Object[]{"",
                                                            prefix.setClickEvent(cv).setHoverEvent(hv),
                                                            new ChatText(" /friends remove [joueur]").setHoverEvent(hv).setClickEvent(cv).setItalic(true).setColor(Colors.GOLD)})));
                        } else if (strings[1].equalsIgnoreCase("accept")
                                || strings[1].equalsIgnoreCase("acept")) {
                            HoverEvent hv = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText[] {new ChatText("Accepte l'invitation d'un joueur !\n").setColor(Colors.GOLD).setBold(true), new ChatText("/friends accept").setBold(true).setColor(Colors.GREEN)});
                            ClickEvent cv = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends accept ");

                            player.unsafe().sendPacket(
                                    new Chat(
                                            UtilJson.getJsonFromObject(
                                                    new Object[]{"",
                                                            prefix.setClickEvent(cv).setHoverEvent(hv),
                                                            new ChatText(" /friends accept [joueur]").setHoverEvent(hv).setClickEvent(cv).setItalic(true).setColor(Colors.GOLD)})));
                        } else if (strings[1].equalsIgnoreCase("list")) {
                            HoverEvent hv = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText[] {new ChatText("Regarde ta liste d'amis !\n").setColor(Colors.GOLD).setBold(true), new ChatText("/friends list").setBold(true).setColor(Colors.GREEN)});
                            ClickEvent cv = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends list ");

                            player.unsafe().sendPacket(
                                    new Chat(
                                            UtilJson.getJsonFromObject(
                                                    new Object[]{"",
                                                            prefix.setClickEvent(cv).setHoverEvent(hv),
                                                            new ChatText(" /friends list").setHoverEvent(hv).setClickEvent(cv).setItalic(true).setColor(Colors.GOLD)})));
                        } else if (strings[1].equalsIgnoreCase("help")) {
                            HoverEvent hv = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText[] {new ChatText("Regarde l'aide pour une commande !\n").setColor(Colors.GOLD).setBold(true), new ChatText("/friends help").setBold(true).setColor(Colors.GREEN)});
                            ClickEvent cv = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/friends help ");

                            player.unsafe().sendPacket(
                                    new Chat(
                                            UtilJson.getJsonFromObject(
                                                    new Object[]{"",
                                                            prefix.setClickEvent(cv).setHoverEvent(hv),
                                                            new ChatText(" /friends help [commande]").setHoverEvent(hv).setClickEvent(cv).setItalic(true).setColor(Colors.GOLD)})));
                        } else {
                            player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(FriendsManagement.help)));
                        }

                    } else {
                        player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(FriendsManagement.help)));
                    }

                } else {
                    player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(FriendsManagement.help)));
                }


            } else {
                player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(FriendsManagement.help)));
            }

        }

    }
}
