package com.masterpik.bungeecommands.help;

import com.masterpik.api.json.*;
import com.masterpik.api.logger.BungeeLogger;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilJson;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.protocol.packet.Chat;

public class CommandsHelp extends Command {
    public CommandsHelp(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            ChatText prefix = new ChatText(HelpManagement.prefix);

            if (args == null
                    || args.length == 0) {
                HelpManagement.sendHelp(player);
            } else if (args.length >= 1) {
                if (args[0].equalsIgnoreCase("help")
                        || args[0].equalsIgnoreCase("h")
                        || args[0].equalsIgnoreCase("?")
                        || args[0].equalsIgnoreCase("aide")) {
                    HelpManagement.sendHelp(player);
                }
                else if (args[0].equalsIgnoreCase("hub")
                        || args[0].equalsIgnoreCase("lobby")) {
                    ChatText help = new ChatText(HelpManagement.helps.get("hub")).setText(" /hub");
                    Object[] packet = new Object[]{"", new ChatText(prefix).setHoverEvent(help.getHoverEvent()).setClickEvent(help.getClickEvent()), help};
                    player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(packet)));
                }
                else if (args[0].equalsIgnoreCase("msg")
                        || args[0].equalsIgnoreCase("w")) {
                    ChatText help = new ChatText(HelpManagement.helps.get("msg")).setText(" /msg");
                    Object[] packet = new Object[]{"", new ChatText(prefix).setHoverEvent(help.getHoverEvent()).setClickEvent(help.getClickEvent()), help};
                    player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(packet)));
                }
                else if (args[0].equalsIgnoreCase("friends")
                        || args[0].equalsIgnoreCase("friend")
                        || args[0].equalsIgnoreCase("amis")
                        || args[0].equalsIgnoreCase("ami")
                        || args[0].equalsIgnoreCase("f")) {
                    ChatText help = new ChatText(HelpManagement.helps.get("friends")).setText(" /friends");
                    Object[] packet = new Object[]{"", new ChatText(prefix).setHoverEvent(help.getHoverEvent()).setClickEvent(help.getClickEvent()), help};
                    player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(packet)));
                }
                else if (args[0].equalsIgnoreCase("pik")) {
                    ChatText help = new ChatText(HelpManagement.helps.get("pik")).setText(" /pik");
                    Object[] packet = new Object[]{"", new ChatText(prefix).setHoverEvent(help.getHoverEvent()).setClickEvent(help.getClickEvent()), help};
                    player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(packet)));
                }
                else if (args[0].equalsIgnoreCase("stats")
                        ||args[0].equalsIgnoreCase("stat")
                        || args[0].equalsIgnoreCase("statistiques")
                        || args[0].equalsIgnoreCase("statistique")
                        || args[0].equalsIgnoreCase("statistics")
                        || args[0].equalsIgnoreCase("statistic")) {
                    ChatText help = new ChatText(HelpManagement.helps.get("stats")).setText(" /stats");
                    Object[] packet = new Object[]{"", new ChatText(prefix).setHoverEvent(help.getHoverEvent()).setClickEvent(help.getClickEvent()), help};
                    player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(packet)));
                }
            }

        }


    }
}
