package com.masterpik.bungeecommands.help;

import com.masterpik.api.json.*;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilJson;
import com.masterpik.bungeecommands.Main;
import com.masterpik.bungeecommands.aliases.Aliases;
import com.masterpik.bungeecommands.privatemessages.CommandsMessages;
import com.masterpik.messages.bungee.Api;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.PluginManager;
import net.md_5.bungee.protocol.packet.Chat;

import java.util.ArrayList;
import java.util.HashMap;

public class HelpManagement {

    public static ChatText prefix;

    public static HashMap<String, ChatText> helps;

    public static void helpInit() {

        initCommmands();

        prefix = new ChatText("▌").setExtra(new Object[]{"", new ChatText("HELP").setBold(true).setColor(Colors.YELLOW), "▌"}).setBold(true).setColor(Colors.BLACK);

        helps = new HashMap<>();
        addHelps();
    }

    public static void initCommmands() {
        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();

        String name = "help";

        ArrayList<String> alias = new ArrayList<>();
        alias.add("aide");
        alias.add("h");
        alias.add("?");
        alias.add(name);
        for (String cmd : alias) {
            pluginManager.registerCommand(Main.plugin, new CommandsHelp(cmd));
        }
        alias.remove(name);

        Aliases.addCommand(name, alias);
    }

    public static void sendHelp(ProxiedPlayer player) {

        ChatText newPrefix = new ChatText(HelpManagement.prefix);

        ArrayList<Object> generalHelps = new ArrayList<>();

        generalHelps.add("");

        generalHelps.add(new ChatText(newPrefix).
                setClickEvent(new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/help ")).
                setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/help").setBold(true).setColor(Colors.GREEN))));

        generalHelps.addAll(helps.values());

        //Object[] help = new Object[]{""};

        player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(generalHelps.toArray())));
    }

    public static void addHelps() {
        helps.put("hub", new ChatText("\n - /hub").
                setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText[] {new ChatText("Retourne au HUB\n").setColor(Colors.YELLOW).setBold(true), new ChatText("/hub").setBold(true).setColor(Colors.GREEN)})).
                setClickEvent(new ClickEvent(ClickEvents.RUN_COMMAND, "/hub")).setItalic(true).setColor(Colors.YELLOW));

        helps.put("msg", new ChatText("\n - /msg").
                setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText[]{new ChatText("Envoie un message privée\n").setColor(Colors.YELLOW).setBold(true), new ChatText("/msg").setBold(true).setColor(Colors.GREEN)})).
                setClickEvent(new ClickEvent(ClickEvents.RUN_COMMAND, "/msg")).setItalic(true).setColor(Colors.YELLOW));

        helps.put("friends", new ChatText("\n - /friends").
                setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText[] {new ChatText("Gere ta liste d'amis\n").setColor(Colors.YELLOW).setBold(true), new ChatText("/friends").setBold(true).setColor(Colors.GREEN)})).
                setClickEvent(new ClickEvent(ClickEvents.RUN_COMMAND, "/friends")).setItalic(true).setColor(Colors.YELLOW));

        helps.put("pik", new ChatText("\n - /pik").
                setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText[] {new ChatText("Regarde tes points PIK\n").setColor(Colors.YELLOW).setBold(true), new ChatText("/pik").setBold(true).setColor(Colors.GREEN)})).
                setClickEvent(new ClickEvent(ClickEvents.RUN_COMMAND, "/pik")).setItalic(true).setColor(Colors.YELLOW));

        helps.put("stats", new ChatText("\n - /stats").
                setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText[] {new ChatText("Regarde tes statistiques de jeux\n").setColor(Colors.YELLOW).setBold(true), new ChatText("/stats").setBold(true).setColor(Colors.GREEN)})).
                setClickEvent(new ClickEvent(ClickEvents.RUN_COMMAND, "/stats")).setItalic(true).setColor(Colors.YELLOW));

    }
}
