package com.masterpik.bungeecommands.aliases;

import com.masterpik.bungeecommands.Main;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.PluginManager;

import java.util.ArrayList;
import java.util.HashMap;

public class Aliases {

    private static HashMap<String, ArrayList<String>> aliases;
    private static HashMap<String, String> names;

    public static void aliasesInit() {
        aliases = new HashMap<>();
        names = new HashMap<>();
    }

    public static void addCommand(String name, ArrayList<String> alias) {
        if (aliases.containsKey(name)) {
            aliases.get(name).addAll(alias);
        } else {
            aliases.put(name, new ArrayList<>());
            aliases.get(name).addAll(alias);

            for (String str : alias) {
                names.put(str, name);
            }
        }
    }

    public static String getCommandFromAlias(String alias) {
        String cmd = ""+alias;

        if (names.containsKey(alias)) {
            cmd = names.get(alias);
        }

        return cmd;
    }

    public static ArrayList<String> getAliases(String name) {
        ArrayList<String> alias = new ArrayList<>();
        if (aliases.containsKey(name)) {
            alias.addAll(aliases.get(name));
        }
        return alias;
    }

}
