package com.masterpik.bungeecommands.privatemessages;

import com.masterpik.api.json.*;
import com.masterpik.api.texts.Colors;
import com.masterpik.bungeecommands.Main;
import com.masterpik.bungeecommands.aliases.Aliases;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.PluginManager;

import java.util.ArrayList;

public class MessagesManagement {
    public static ChatText prefix;
    public static ChatText help;

    public static void MessagesInit() {

        initCommmands();

        prefix = new ChatText("▌").setExtra(new Object[]{"", new ChatText("MP").setBold(true).setColor(Colors.AQUA), "▌"}).setBold(true).setColor(Colors.BLACK);

        help = new ChatText(" /msg [joueur] [message]").setItalic(true).setColor(Colors.AQUA).setClickEvent(new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/msg ")).setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/msg").setColor(Colors.GREEN).setBold(true)));

    }

    public static void initCommmands() {
        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();

        String name = "msg";

        ArrayList<String> alias = new ArrayList<>();
        alias.add("w");
        alias.add("mp");
        alias.add(name);
        for (String cmd : alias) {
            pluginManager.registerCommand(Main.plugin, new CommandsMessages(cmd));
        }
        alias.remove(name);

        Aliases.addCommand(name, alias);
    }

}
