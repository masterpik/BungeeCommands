package com.masterpik.bungeecommands.privatemessages;

import com.masterpik.api.json.*;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilJson;
import com.masterpik.bungee.taktik.events.ChatEvents;
import com.masterpik.bungeecommands.Main;
import com.masterpik.bungeemanagement.mute.MuteManagement;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.protocol.packet.Chat;

public class CommandsMessages extends Command {
    public CommandsMessages(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {

        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            if (!MuteManagement.isMute(player.getUniqueId())) {

                if (strings != null
                        && strings.length > 0) {

                    if (ProxyServer.getInstance().getPlayer(strings[0]) != null) {

                        ProxiedPlayer destPlayer = ProxyServer.getInstance().getPlayer(strings[0]);

                        if (destPlayer != player) {

                            ClickEvent pclick = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/msg " + destPlayer.getName() + " ");
                            HoverEvent phover = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/msg " + destPlayer.getName() + " ").setColor(Colors.GREEN).setBold(true));
                            ClickEvent dclick = new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/msg " + player.getName() + " ");
                            HoverEvent dhover = new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText("/msg " + player.getName() + " ").setColor(Colors.GREEN).setBold(true));


                            ChatText pprefix = new ChatText(MessagesManagement.prefix);
                            pprefix.setHoverEvent(phover);
                            pprefix.setClickEvent(pclick);

                            ChatText dprefix = new ChatText(MessagesManagement.prefix);
                            dprefix.setHoverEvent(dhover);
                            dprefix.setClickEvent(dclick);

                            if (strings.length >= 2) {

                                String message = "";

                                int bucle = 1;

                                while (bucle < strings.length) {

                                    if (bucle == 1) {
                                        message = strings[bucle];
                                    } else {
                                        message = message + " " + strings[bucle];
                                    }

                                    bucle++;
                                }
                                message = " " + message;

                                if (!ChatEvents.isBadChat(message, player.getName())) {


                                    ChatText pmsg1 = new ChatText(" " + player.getName());
                                    pmsg1.setColor(Colors.GOLD);
                                    pmsg1.setExtra(new Object[]{"", new ChatText(" > ").setColor(Colors.GREEN), destPlayer.getName()});
                                    pmsg1.setHoverEvent(phover);
                                    pmsg1.setClickEvent(pclick);


                                    ChatText dmsg1 = new ChatText(" " + player.getName());
                                    dmsg1.setColor(Colors.GOLD);
                                    dmsg1.setExtra(new Object[]{"", new ChatText(" > ").setColor(Colors.GREEN), destPlayer.getName()});
                                    dmsg1.setHoverEvent(dhover);
                                    dmsg1.setClickEvent(dclick);

                                    ChatText msg2 = new ChatText(message);
                                    msg2.setItalic(true);
                                    msg2.setColor(Colors.AQUA);


                                    String pmsg = UtilJson.getJsonFromObject(new Object[]{"", pprefix, pmsg1, msg2});
                                    String dmsg = UtilJson.getJsonFromObject(new Object[]{"", dprefix, dmsg1, msg2});

                                    player.unsafe().sendPacket(new Chat(pmsg));
                                    destPlayer.unsafe().sendPacket(new Chat(dmsg));
                                } else {
                                    player.sendMessage(new TextComponent(ChatEvents.badChat));
                                }

                            /*player.sendMessages("[Message Privée] (" + destPlayer.getName() + ") > " + message);
                            destPlayer.sendMessages("[Message Privée] (" + player.getName() + ") > " + message);*/

                            } else {
                                player.unsafe().sendPacket(
                                        new Chat(
                                                UtilJson.getJsonFromObject(
                                                        new Object[]{"",
                                                                pprefix,
                                                                new ChatText(" Ton message est vide").setColor(Colors.RED).setBold(true)})));
                                //player.sendMessages("[Message Privée] syntax error 003");
                            }

                        } else {
                            player.unsafe().sendPacket(
                                    new Chat(
                                            UtilJson.getJsonFromObject(
                                                    new Object[]{"",
                                                            MessagesManagement.prefix.setClickEvent(MessagesManagement.help.getClickEvent()).setHoverEvent(MessagesManagement.help.getHoverEvent()),
                                                            new ChatText(" Tu ne peux pas t'envoyer un message").setColor(Colors.RED).setBold(true)})));
                            //player.sendMessages("[Message Privée] syntax error 004");
                        }

                    } else if (strings[0].equalsIgnoreCase("help")) {
                        player.unsafe().sendPacket(
                                new Chat(
                                        UtilJson.getJsonFromObject(
                                                new Object[]{"",
                                                        MessagesManagement.prefix.setClickEvent(MessagesManagement.help.getClickEvent()).setHoverEvent(MessagesManagement.help.getHoverEvent()),
                                                        MessagesManagement.help})));
                    } else {
                        player.unsafe().sendPacket(
                                new Chat(
                                        UtilJson.getJsonFromObject(
                                                new Object[]{"",
                                                        MessagesManagement.prefix.setClickEvent(MessagesManagement.help.getClickEvent()).setHoverEvent(MessagesManagement.help.getHoverEvent()),
                                                        new ChatText(" Le joueur ").setExtra(
                                                                new Object[]{"",
                                                                        new ChatText(strings[0]).setColor(Colors.AQUA).setBold(false).setItalic(true),
                                                                        " n'est pas connecté au serveur"}).setBold(true).setColor(Colors.RED)})));
                        //player.sendMessages("[Message Privée] syntax error 002");
                    }
                } else {
                    player.unsafe().sendPacket(
                            new Chat(
                                    UtilJson.getJsonFromObject(
                                            new Object[]{"",
                                                    MessagesManagement.prefix.setClickEvent(MessagesManagement.help.getClickEvent()).setHoverEvent(MessagesManagement.help.getHoverEvent()),
                                                    MessagesManagement.help})));
                    //player.sendMessages("[Message Privée] syntax error 001");
                }

            } else {
                player.sendMessage(new TextComponent(MuteManagement.getMuteMsg(MuteManagement.getMuteInfo(player.getUniqueId()))));
            }


        }

    }
}
