package com.masterpik.bungeecommands;

import com.masterpik.api.json.ChatText;
import com.masterpik.api.texts.Colors;
import com.masterpik.bungeecommands.aliases.Aliases;
import com.masterpik.bungeecommands.friends.CommandsFriends;
import com.masterpik.bungeecommands.friends.FriendsManagement;
import com.masterpik.bungeecommands.help.CommandsHelp;
import com.masterpik.bungeecommands.help.HelpManagement;
import com.masterpik.bungeecommands.hub.HubManagement;
import com.masterpik.bungeecommands.party.PartyManagement;
import com.masterpik.bungeecommands.privatemessages.CommandsMessages;
import com.masterpik.bungeecommands.privatemessages.MessagesManagement;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

public class Main extends Plugin {

    public static Plugin plugin;

    public void onEnable() {
        plugin = this;

        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();

        pluginManager.registerListener(this, new Events());

        Aliases.aliasesInit();

        HubManagement.hubInit();
        MessagesManagement.MessagesInit();
        FriendsManagement.FriendsInit();
        PartyManagement.PartyInit();
        HelpManagement.helpInit();



    }

}
